class ResultService {
    parseResult(result) {
        return {
            duration: result.timing.total,
            metrics: result.audits.metrics.details.items[0]
        };
    }
}

export default ResultService;
