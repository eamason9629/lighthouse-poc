// import LighthouseService from './service/LighthouseService';
// import ResultService from './service/ResultService';
// import fs from 'fs';
//
// const url = 'https://www.google.com';
// // const url = 'https://www.bestbuy.com';
//
// const lighthouseService = new LighthouseService();
// const resultService = new ResultService();
//
// const lighthouseResult = lighthouseService.runForUrl(url);
// const transformedResult = resultService.parseResult(lighthouseResult);
// fs.writeFile(`./results/result-${new Date()}.json`, JSON.stringify(transformedResult), function (err) {
//     if (err) {
//         return console.log(err);
//     }
//
//     console.log('The file was saved!');
// });

import lighthouse from 'lighthouse';
import chromeLauncher from "chrome-launcher";
// const lighthouse = require('lighthouse');
// const chromeLauncher = require('chrome-launcher');
import fs from 'fs';
import ResultService from './service/ResultService';

function launchChromeAndRunLighthouse(url, opts, config = null) {
    return chromeLauncher.launch({chromeFlags: opts.chromeFlags}).then(chrome => {
        opts.port = chrome.port;
        return lighthouse(url, opts, config).then(results => {
            // use results.lhr for the JS-consumeable output
            // https://github.com/GoogleChrome/lighthouse/blob/master/typings/lhr.d.ts
            // use results.report for the HTML/JSON/CSV output as a string
            // use results.artifacts for the trace/screenshots/other specific case you need (rarer)
            return chrome.kill().then(() => results.lhr)
        });
    });
}

const url = 'https://www.google.com';

const opts = {
    chromeFlags: ['--show-paint-rects'],
    onlyCategories: ['performance']
};

const resultService = new ResultService();

// Usage:
launchChromeAndRunLighthouse(url, opts).then(results => {
    let transformedResult = resultService.parseResult(results);
    fs.writeFile(`./results/result-${new Date()}.json`, JSON.stringify(transformedResult), function (err) {
        if (err) {
            return console.log(err);
        }

        console.log('The file was saved!');
    });
});