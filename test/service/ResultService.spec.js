import fs from 'fs';
import ResultService from '../../src/service/ResultService';

describe('testing ResultService', () => {
    const sut = new ResultService();
    const given = JSON.parse(fs.readFileSync('./test/service/ResultService-given.json', 'utf8'));
    const expected = JSON.parse(fs.readFileSync('./test/service/ResultService-expected.json', 'utf8'));

    it('parse result data', () => {
        //when
        const result = sut.parseResult(given);

        //then
        expect(result).toEqual(expected);
    });
});
