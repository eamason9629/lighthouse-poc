import lighthouse from 'lighthouse';
import chromeLauncher from 'chrome-launcher';

const opts = {
    chromeFlags: ['--show-paint-rects'],
    onlyCategories: ['performance']
};

class LighthouseService {
    runForUrl(url) {
        return chromeLauncher.launch({chromeFlags: opts.chromeFlags}).then(chrome => {
            opts.port = chrome.port;
            lighthouse(url, opts).then(results => {
                return chrome.kill().then(() => results.lhr);
            });
        }).then(results => {
            return results;
        });
    }
}

export default LighthouseService;